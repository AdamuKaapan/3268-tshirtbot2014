/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.templates.RobotMap;
import edu.wpi.first.wpilibj.templates.commands.CameraJoystickControl;

public class CameraSubsystem extends Subsystem
{
    public double cameraX, cameraY;
    
    public final double cameraXDefault = 0.5, cameraYDefault = 0.5;
    
    public void resetCamera()
    {
        cameraX = cameraXDefault;
        cameraY = cameraYDefault;
    }
    
    public void updateCamera()
    {
        if (cameraX < 0.0)
            cameraX = 0.0;
        if (cameraX > 1.0)
            cameraX = 1.0;
        
        if (cameraY < 0.0)
            cameraY = 0.0;
        if (cameraY > 1.0)
            cameraY = 1.0;
        
        RobotMap.cameraXServo.set(cameraX);
        RobotMap.cameraYServo.set(cameraY);
    }

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        setDefaultCommand(new CameraJoystickControl());
    }
}
