package edu.wpi.first.wpilibj.templates.subsystems;

import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.RobotMap;
import edu.wpi.first.wpilibj.templates.commands.Drive;

public class Drivebase extends Subsystem
{
    private double previousLeftSpeed, previousRightSpeed;
    private double currentLeftSpeed, currentRightSpeed;
    
    public void putSpeedsToSmartDashboard()
    {
        SmartDashboard.putNumber("Left Wheel Speed", RobotMap.leftDriveMotor.get());
        SmartDashboard.putNumber("Right Wheel Speed", RobotMap.rightDriveMotor.get());
    }
    
    public void restrictSpeeds()
    {
        previousLeftSpeed = currentLeftSpeed;
        previousRightSpeed = currentRightSpeed;
        
        currentLeftSpeed = RobotMap.leftDriveMotor.get();
        currentRightSpeed = RobotMap.rightDriveMotor.get();
        
        double maxDelta = SmartDashboard.getNumber("Max Motor Delta");
        
        if (Math.abs(previousLeftSpeed - currentLeftSpeed) > maxDelta)
        {
            RobotMap.leftDriveMotor.set(previousLeftSpeed - maxDelta);
        }
        
        if (Math.abs(previousRightSpeed - currentRightSpeed) > maxDelta)
        {
            RobotMap.rightDriveMotor.set(previousRightSpeed - maxDelta);
        }
    }
    
    public RobotDrive driver = new RobotDrive(RobotMap.leftDriveMotor, RobotMap.rightDriveMotor);

    public void initDefaultCommand()
    {
        // Set the default command for a subsystem here.
        setDefaultCommand(new Drive());
    }
}
