package edu.wpi.first.wpilibj.templates;

import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.camera.AxisCamera;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.commands.CommandBase;

public class TShirtMain extends IterativeRobot
{
    Command autonomousCommand;

    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    public void robotInit()
    {
        // instantiate the command used for the autonomous period
        
        //autonomousCommand = new ExampleCommand();

        // Place all smart dashboard variables
        SmartDashboard.putNumber("Precision Drive Modifier", 0.75);
        SmartDashboard.putNumber("Left Wheel Speed", 0.0);
        SmartDashboard.putNumber("Right Wheel Speed", 0.0);
        //SmartDashboard.putNumber("Max Motor Delta", 0.01);
        
        // Initialize all subsystems
        CommandBase.init();
        
        AxisCamera.getInstance().writeMaxFPS(64);
    }

    public void autonomousInit()
    {
        // schedule the autonomous command (example)
        
        //autonomousCommand.start();
    }

    /**
     * This function is called periodically during autonomous
     */
    public void autonomousPeriodic()
    {
        Scheduler.getInstance().run();
    }

    public void teleopInit()
    {
        // This makes sure that the autonomous stops running when
        // teleop starts running. If you want the autonomous to 
        // continue until interrupted by another command, remove
        // this line or comment it out.
        //autonomousCommand.cancel();
    }

    /**
     * This function is called periodically during operator control
     */
    public void teleopPeriodic()
    {
        Scheduler.getInstance().run();
    }

    /**
     * This function is called periodically during test mode
     */
    public void testPeriodic()
    {
        LiveWindow.run();
    }
}
