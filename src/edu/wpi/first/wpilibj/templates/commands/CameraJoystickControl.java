/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.commands;

public class CameraJoystickControl extends CommandBase
{
    double speedModifier;
    
    public CameraJoystickControl()
    {
        // Use requires() here to declare subsystem dependencies
        requires(cameraSubsystem);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        // On Attack3 Joysticks
//        if (CommandBase.oi.rightJoystick.getRawButton(4))
//            cameraSubsystem.cameraX += 0.01;
//        if (CommandBase.oi.rightJoystick.getRawButton(5))
//            cameraSubsystem.cameraX -= 0.01;
//        if (CommandBase.oi.rightJoystick.getRawButton(3))
//            cameraSubsystem.cameraY += 0.01;
//        if (CommandBase.oi.rightJoystick.getRawButton(2))
//            cameraSubsystem.cameraY -= 0.01;
        
        // STILL HAS TO BE TESTED FOR CORRECT DIRECTION!
        cameraSubsystem.cameraX -= CommandBase.oi.rightJoystick.getRawAxis(5) * speedModifier;
        cameraSubsystem.cameraY -= CommandBase.oi.rightJoystick.getRawAxis(6) * speedModifier;
        
        if (CommandBase.oi.rightJoystick.getRawButton(2)) // Button 8 on Attack3
            cameraSubsystem.resetCamera();
        
        cameraSubsystem.updateCamera();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }

    // Called once after isFinished returns true
    protected void end()
    {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
