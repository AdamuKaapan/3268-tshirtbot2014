package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.OI;
import edu.wpi.first.wpilibj.templates.subsystems.CameraSubsystem;
import edu.wpi.first.wpilibj.templates.subsystems.CompressorSubsystem;
import edu.wpi.first.wpilibj.templates.subsystems.Drivebase;
import edu.wpi.first.wpilibj.templates.subsystems.FiringMechanism;

/**
 * The base for all commands. All atomic commands should subclass CommandBase.
 * CommandBase stores creates and stores each control system. To access a
 * subsystem elsewhere in your code in your code use CommandBase.exampleSubsystem
 * @author Author
 */
public abstract class CommandBase extends Command {

    public static OI oi;
    // Create a single static instance of all of your subsystems
    public static Drivebase drivebase = new Drivebase();
    
    public static CompressorSubsystem compressor = new CompressorSubsystem();
    
    public static FiringMechanism firingMechanism = new FiringMechanism();
    
    public static CameraSubsystem cameraSubsystem = new CameraSubsystem();
    
    public static void init() {
        // DO NOT MOVE
        oi = new OI();

        // Show what command your subsystem is running on the SmartDashboard
        SmartDashboard.putData(drivebase);
        SmartDashboard.putData(compressor);
        SmartDashboard.putData(firingMechanism);
        SmartDashboard.putData(cameraSubsystem);
        
        cameraSubsystem.resetCamera();
    }

    public CommandBase(String name) {
        super(name);
    }

    public CommandBase() {
        super();
    }
}
