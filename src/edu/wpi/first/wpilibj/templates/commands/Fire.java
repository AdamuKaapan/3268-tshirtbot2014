/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.templates.RobotMap;

/**
 *
 * @author PowerStudent
 */
public class Fire extends CommandBase
{

    public Fire()
    {
        // Use requires() here to declare subsystem dependencies
        requires(firingMechanism);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        RobotMap.firingRelay.set(Relay.Value.kForward);
        this.setTimeout(0.5);
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return this.isTimedOut();
    }

    // Called once after isFinished returns true
    protected void end()
    {
        RobotMap.firingRelay.set(Relay.Value.kOff);
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
    }
}
