package edu.wpi.first.wpilibj.templates.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.templates.RobotMap;

public class Drive extends CommandBase
{
    
    public Drive()
    {
        // Use requires() here to declare subsystem dependencies
        requires(drivebase);
    }

    // Called just before this Command runs the first time
    protected void initialize()
    {
        
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute()
    {
        drivebase.driver.tankDrive(-CommandBase.oi.leftJoystick.getY(), -CommandBase.oi.rightJoystick.getY());
        drivebase.putSpeedsToSmartDashboard();
        //drivebase.restrictSpeeds();
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished()
    {
        return false;
    }

    // Called once after isFinished returns true
    protected void end()
    {
        drivebase.driver.tankDrive(0.0, 0.0);
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted()
    {
        drivebase.driver.tankDrive(0.0, 0.0);
    }
}
